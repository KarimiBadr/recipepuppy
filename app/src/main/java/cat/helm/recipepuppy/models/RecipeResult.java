
package cat.helm.recipepuppy.models;

import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class RecipeResult {

    @SerializedName("href")
    private String mHref;
    @SerializedName("results")
    private List<Recipe> mRecipes;
    @SerializedName("title")
    private String mTitle;
    @SerializedName("version")
    private Double mVersion;

    public String getHref() {
        return mHref;
    }

    public void setHref(String href) {
        mHref = href;
    }

    public List<Recipe> getResults() {
        return mRecipes;
    }

    public void setResults(List<Recipe> recipes) {
        mRecipes = recipes;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public Double getVersion() {
        return mVersion;
    }

    public void setVersion(Double version) {
        mVersion = version;
    }

}

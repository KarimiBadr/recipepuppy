package cat.helm.recipepuppy

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import cat.helm.recipepuppy.api.RecipeRepository
import cat.helm.recipepuppy.ui.RecipesListActivity
import cat.helm.recipepuppy.utils.PreferenceHelper

import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    val repository = RecipeRepository(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        startActivity(Intent(this, RecipesListActivity::class.java))
        getRecipeBtn.setOnClickListener {
            repository.getRecipes()
        }

        var str = PreferenceHelper.defaultPrefs(this).getString("nom","tkhhhhhhhhhh")
        Log.e("MainActivity", str)

        PreferenceHelper.defaultPrefs(this).edit().putString("nom","badr").apply()

    }




}

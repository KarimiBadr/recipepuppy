package cat.helm.recipepuppy.api

import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

/**
 * Created by HP on 6/24/2018.
 */
class RecipeClient {

    var recipeService: RecipeService
    private var URL_BASE = "http://recipepuppy.com/"

    init {
        val retrofit = Retrofit.Builder()
                .baseUrl(URL_BASE)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()
        recipeService = retrofit.create(RecipeService::class.java)
    }

    companion object {
        private val recipeResultClient = RecipeClient()

        @Synchronized
        fun getInstance(): RecipeClient = recipeResultClient
    }
}
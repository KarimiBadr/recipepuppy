package cat.helm.recipepuppy.api

import cat.helm.recipepuppy.models.RecipeResult
import io.reactivex.Observable
import retrofit2.http.GET

/**
 * Created by HP on 6/24/2018.
 */
interface RecipeService {

    @GET("api/")
    fun getRecipes(): Observable<RecipeResult>
}
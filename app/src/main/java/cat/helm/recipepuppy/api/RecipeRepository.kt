package cat.helm.recipepuppy.api

import android.content.Context
import android.util.Log
import cat.helm.recipepuppy.models.Recipe
import cat.helm.recipepuppy.models.RecipeResult
import cat.helm.recipepuppy.utils.PreferenceHelper
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

/**
 * Created by HP on 6/24/2018.
 */
class RecipeRepository(val context: Context){

    val recipeClient = RecipeClient.getInstance()

    fun getRecipes() = recipeClient.recipeService.getRecipes()

    fun setBookmarkRecipe(recipe: Recipe){
        // identify the recipe with "href"
        PreferenceHelper.defaultPrefs(context).edit().putBoolean(recipe.href,true).apply()
    }

    fun removeBookmarkRecipe(recipe: Recipe){
        PreferenceHelper.defaultPrefs(context).edit().putBoolean(recipe.href,false).apply()
    }

    fun isRecipeBookmarked(recipe: Recipe): Boolean = PreferenceHelper.defaultPrefs(context).getBoolean(recipe.href,false)

//    fun getRecipes(){
//        recipeClient.recipeService.getRecipes()
//                .subscribeOn(Schedulers.io())
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribe(
//                        {
//                            t: RecipeResult ->
//                            showList(t.results)
//                        }
//                )
//    }

    fun showList(recipes : List<Recipe>){
        for (recipe in recipes){
            Log.e("RecipeRepository" , recipe.title)
        }
    }

}
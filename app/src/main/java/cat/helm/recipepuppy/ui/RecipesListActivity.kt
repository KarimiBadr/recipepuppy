package cat.helm.recipepuppy.ui

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import cat.helm.recipepuppy.R
import cat.helm.recipepuppy.api.RecipeRepository
import cat.helm.recipepuppy.models.Recipe
import cat.helm.recipepuppy.models.RecipeResult
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_recipes_list.*

class RecipesListActivity : AppCompatActivity() {

    val repository = RecipeRepository(this)
    lateinit var recipes :ArrayList<Recipe>

    lateinit var adapter: RecipesAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_recipes_list)

    }

    override fun onStart() {
        super.onStart()

        repository.getRecipes()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        {
                            t: RecipeResult ->
                            recipes = t.results as ArrayList<Recipe>
                            initRecyclerView(recipes)
                        }
                )
    }

    fun initRecyclerView(recipes: ArrayList<Recipe>){
        adapter = RecipesAdapter(recipes){
            Log.e("RecipesListActivity","..." )
        }

        recycler_view.layoutManager = GridLayoutManager(this,2)
        recycler_view.adapter = adapter

    }
}

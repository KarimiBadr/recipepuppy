package cat.helm.recipepuppy.ui

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import cat.helm.recipepuppy.R
import cat.helm.recipepuppy.api.RecipeRepository
import cat.helm.recipepuppy.models.Recipe
import com.squareup.picasso.NetworkPolicy
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_recipe_details.*
import kotlinx.android.synthetic.main.recipe_card.view.*

class RecipeDetailsActivity : AppCompatActivity() {

    val repository = RecipeRepository(this)
    lateinit var recipe: Recipe

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_recipe_details)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        recipe = intent.getSerializableExtra("recipe_object") as Recipe

        init()

        configListeners()
    }

    fun init(){
        Picasso.with(this).load(recipe.thumbnail).networkPolicy(NetworkPolicy.NO_CACHE).into(imageTop)
        titleTextView.text = recipe.title
        ingredientsTextView.text = recipe.ingredients
        configFavorite()
    }

    fun configFavorite(){
        if(repository.isRecipeBookmarked(recipe)){
            Picasso.with(this).load(R.drawable.h_full).into(bookmarkIcon)
        }else{
            Picasso.with(this).load(R.drawable.h_empty).into(bookmarkIcon)
        }
    }

    fun configListeners(){

        bookmarkIcon.setOnClickListener {

            if(repository.isRecipeBookmarked(recipe))
                repository.removeBookmarkRecipe(recipe)
            else
                repository.setBookmarkRecipe(recipe)

            configFavorite()
        }
    }


    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        var id = item?.itemId
        if(id == android.R.id.home){
            finish()
        }
        return super.onOptionsItemSelected(item)
    }
}

package cat.helm.recipepuppy.ui

import android.app.Activity
import android.content.Intent
import android.support.v7.widget.CardView
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import cat.helm.recipepuppy.R
import cat.helm.recipepuppy.api.RecipeRepository
import cat.helm.recipepuppy.models.Recipe
import com.squareup.picasso.NetworkPolicy
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.recipe_card.view.*

/**
 * Created by HP on 6/24/2018.
 */
class RecipesAdapter(val items: ArrayList<Recipe>, val listener: (Int) -> Unit): RecyclerView.Adapter<RecipesAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.recipe_card, parent, false))

    override fun getItemCount() = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bind(items[position], position, listener)

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {



        fun bind(item: Recipe, pos: Int, listener: (Int) -> Unit) = with(itemView) {

            val repository = RecipeRepository(context)

            var intent = Intent(context, RecipeDetailsActivity::class.java)

            recipe_image.setOnClickListener {
                Log.e("RecipesAdapter",".")
                listener(pos)

                intent.putExtra("recipe_object",item)
                context.startActivity(intent)
            }

            Picasso.with(context).load(item.thumbnail).networkPolicy(NetworkPolicy.NO_CACHE).into(recipe_image)

            recipe_title.text = item.title

            if(repository.isRecipeBookmarked(item)){
                Picasso.with(context).load(R.drawable.h_full).into(bookmarkSubIcon)
            }


        }
    }
}